FROM openjdk:8u111-jdk-alpine

ENV JAVA_OPTS '-Xmx512m -Xms512m'

VOLUME /tmp
ADD /target/image-resizer.jar app.jar
ADD docker-entrypoint.sh docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]